# CETATOCONE DJANGO WEBSITE

### Dependências

Dependências de projeto gerenciadas via [**pipenv**](https://pypi.org/project/pipenv/) rodando no **Python 3.8**. Consulte o arquivo `Pipfile` para detalhes.

- [django](https://pypi.org/project/Django/)
- [django-storages](https://pypi.org/project/django-storages/)
- [pillow](https://pypi.org/project/Pillow/)
- [dropbox](https://pypi.org/project/dropbox/)
- [django-heroku](https://pypi.org/project/django-heroku/)
- [gunicorn](https://pypi.org/project/gunicorn/)

### Servindo o site localmente

Instale o pipenv para criar o ambiente virtual.

```shell
$ pip install pipenv
```

Baixe as dependências do projeto.

```shell
$ pipenv install
```

Acesse o ambiente virtual do pipenv;

```shell
$ pipenv shell
```

Agora execute os seguintes comandos relativos do Django para preparar a execução.

```shell
$ ./manage.py makemigrations
$ ./manage.py migrate
$ ./manage.py migrate --run-syncdb
```

Para executar o projeto apenas execute o seguinte comando.

```shell
$ ./manage.py runserver
```

### Comentário sobre o deploy

Este projeto está pronto para rodar no [**Heroku**](https://heroku.com/). O pacote [**gunicorn**](https://pypi.org/project/gunicorn/) serve o WSGI em produção. O dyno consta no arquivo `Procfile`.

```
web: gunicorn ceratocone.wsgi
```

O pacote [**django-heroku**](https://pypi.org/project/django-heroku/) automatiza o processo de criação de banco de dados para a aplicação online. Ao fazer o deploy, o site automaticamente passará a usar um banco de dados **PostgreSQL** servido através do serviço [**Heroku Postgres**](https://elements.heroku.com/addons/heroku-postgresql).

O DNS do site também pode ser facilmente configurado através do serviço [**PointDNS**](https://elements.heroku.com/addons/pointdns). Este serviço pode ser instalado através da dashboard ou do cli do Heroku.

**Caso opte por não utilizar o Heroku para rodar o site**, você pode simplesmente remover o pacote **django-heroku** através do seguinte comando.

```shell
$ pipenv uninstall django-heroku
```

Também edite o arquivo `ceratocone/settings.py` e remova as chamadas ao pacote.

```python
# REMOVA O DJANGO_HEROKU DOS IMPORTS NO COMEÇO DO ARQUIVO
import os, django_heroku

...

# REMOVA A CHAMADA DO MÓDULO NA ÚLTIMA LINHA DO ARQUIVO
django_heroku.settings(locals())
```

### Banco de dados

O projeto utiliza localmente **SQLite** para agilizar o desenvolvimento. O arquivo é o `db.sqlite3`. Como já dito no tópico anterior, ao fazer deploy para o Heroku, o site passará a utilizar automáticamente um banco de dados PostgreSQL.

**Caso opte por não utilizar o Heroku para rodar o site**, ou mesmo apenas mudar as configurações de banco de dados, você deve modificar o arquivo `ceratocone/settings.py`.

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```

Em **Python**, recomenda-se o uso de um arquivo `.env` para armazenar as variáveis do banco de dados. Arquivos `.env` podem ser facilmente carregados utilizando o módulo [**python-dotenv**](https://pypi.org/project/python-dotenv/).

### Serviço de armazenamento de arquivos

Por ter sido feito pensando no **Heroku**, o projeto utiliza o **Dropbox** para armazenar arquivos enviados pelos usuários do site através do pacote [**django-storages**](https://pypi.org/project/django-storages/). Isto pode ser facilmente alterado no final do arquivo `ceratocone/settings.py`.

```python
# DEFINE O SERVIÇO DE ARMAZENAMENTO UTILIZADO
DEFAULT_FILE_STORAGE = 'storages.backends.dropbox.DropBoxStorage'

# TOKEN DE APP GERADO NO CONSOLE DO DROPBOX DEVELOPERS
DROPBOX_OAUTH2_TOKEN = ''
```

Consulte a documentação do **django-storages** [aqui](https://django-storages.readthedocs.io/en/latest/) para ver os serviços disponíveis caso deseje utilizar outro. Se decidir não utilizar o Dropbox, não esqueça de remover o módulo do projeto.

```shell
$ pipenv uninstall dropbox
```

**Caso opte por não utilizar o Heroku para rodar o site** e utilize um serviço de hospedagem que dispõe de um serviço de armazenamento, você pode simplesmente remover o módulo **django-storages**.

```shell
$ pipenv uninstall django-storages
```

---
Developed by [@jjpaulo2](https://jjpaulo2.github.io).