from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('page/sobre/', views.sobre, name='sobre'),
    path('page/depoimentos/', views.depoimentos_index, name='depoimentos'),
    path('page/depoimentos/enviar/', views.depoimentos_enviar, name='enviar-depoimento'),
    path('page/depoimentos/<int:pagina>/', views.depoimentos, name='depoimentos'),
    path('page/doar/', views.doar, name='doar'),
    path('page/contato/', views.contato, name='contato')
]