from django.db import models

class Sobre(models.Model):
    endereco = models.CharField(verbose_name='Endereço físico', max_length=200)

    texto_doe = models.CharField(verbose_name='Texto resumido da seção "DOE" (exibido na página inicial e de contato)', max_length=300)
    texto_nossa_missao = models.CharField(verbose_name='Texto resumido da seção "NOSSA MISSÃO" (exibido na página inicial)', max_length=300)
    texto_sobre_nos = models.CharField(verbose_name='Texto resumido da seção "SOBRE NÓS" (exibido na página inicial)', max_length=300)

    texto_missao = models.TextField(verbose_name='Texto completo da seção "MISSÃO" (exibido na página de sobre)')
    texto_objetivos = models.TextField(verbose_name='Texto completo da seção "OBJETIVOS" (exibido na página de sobre)')
    texto_valores = models.TextField(verbose_name='Texto completo da seção "VALORES" (exibido na página de sobre)')

    def __str__(self):
        return 'EDITAR AS INFORMAÇÕES DO SITE'

class TipoContato(models.Model):
    tipo = models.CharField(max_length=10)
    fonte_class = models.CharField(max_length=50)

    def __str__(self):
        return self.tipo

class Contato(models.Model):
    tipo_contato = models.ForeignKey(TipoContato, on_delete=models.CASCADE)
    contato = models.CharField(verbose_name='Contato', max_length=50)

    def __str__(self):
        return "{}: {}".format(self.tipo_contato, self.contato)

class TipoRedeSocial(models.Model):
    tipo = models.CharField(max_length=10)
    fonte_class = models.CharField(max_length=50)

    def __str__(self):
        return self.tipo

class RedeSocial(models.Model):
    tipo_rede_social = models.ForeignKey(TipoRedeSocial, on_delete=models.CASCADE)
    link = models.CharField(verbose_name='Link para a página', max_length=200)

    def __str__(self):
        return "{}: {}".format(self.tipo_rede_social, self.link)

class Slide(models.Model):
    id = models.AutoField(primary_key=True)
    titulo = models.CharField(verbose_name='Título da imagem', max_length=200)
    imagem = models.ImageField(verbose_name='Imagem (garanta que todas as imagens utilizadas tenham as mesmas dimensões para evitar quebras no layout)', upload_to='slides')

    def __str__(self):
        return "Imagem #{}: {}".format(self.id, self.titulo)

class Depoimento(models.Model):
    nome = models.CharField(verbose_name='Seu nome', max_length=200)
    foto = models.ImageField(verbose_name='Sua foto', upload_to='depoimentos/foto')
    telefone = models.CharField(verbose_name='Seu telefone', max_length=20)
    depoimento = models.TextField(verbose_name='Mensagem de depoimento')
    exibir = models.BooleanField(verbose_name='Exibir no site')

    def __str__(self):
        return 'Depoimento de ' + self.nome