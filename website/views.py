from django.shortcuts import render, redirect
from django.template import loader
from .models import *

def index(request):
    template = 'website/index.html'
    context = {
        'slides': Slide.objects.all().order_by('-id'),
        'paginacao_slides': list(range(Slide.objects.all().count())),
        'sobre': Sobre.objects.all()[0],
        'depoimentos': Depoimento.objects.filter(exibir=True).order_by('-id')[:1],
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
        }
    return render(request, template, context)

def sobre(request):
    template = 'website/pages/sobre.html'
    context = {
        'sobre': Sobre.objects.all()[0],
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
        }
    return render(request, template, context)

def depoimentos_index(request):
    return redirect('../../page/depoimentos/1/', permanent=False)

def depoimentos_enviar(request):
    template = 'website/pages/novo-depoimento.html'
    context = {
        'sobre': Sobre.objects.all()[0],
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
    }
    return render(request, template, context)

def depoimentos(request, pagina):
    template = 'website/pages/depoimentos.html'

    depoimentos_por_pagina = 2
    quant_depoimentos = Depoimento.objects.filter(exibir=True).count()
    quant_paginas = round((quant_depoimentos/depoimentos_por_pagina) + 0.5)
    final = pagina * depoimentos_por_pagina
    inicio = final - depoimentos_por_pagina
    
    proxima_pagina = pagina + 1
    pagina_anterior = pagina - 1
    if pagina == 1:
        pagina_anterior += 1
    elif pagina == quant_paginas:
        proxima_pagina -= 1

    context = {
        'depoimentos': Depoimento.objects.filter(exibir=True).order_by('-id')[inicio:final],
        'quant_depoimentos': quant_depoimentos,
        'pagina': pagina,
        'pagina_anterior': pagina_anterior,
        'proxima_pagina': proxima_pagina,
        'paginacao': list(range(1, quant_paginas+1)),
        'endereco': Sobre.objects.all()[0].endereco,
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
        }
    return render(request, template, context)

def doar(request):
    template = 'website/pages/doar.html'
    context = { 
        'sobre': Sobre.objects.all()[0],
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
        }
    return render(request, template, context)

def contato(request):
    template = 'website/pages/contato.html'
    context = { 
        'sobre': Sobre.objects.all()[0],
        'contatos': Contato.objects.all(),
        'redes_sociais': RedeSocial.objects.all()
        }
    return render(request, template, context)