from django.contrib import admin
from .models import *

admin.site.register(Sobre)
admin.site.register(Depoimento)
admin.site.register(TipoContato)
admin.site.register(TipoRedeSocial)
admin.site.register(Slide)

@admin.register(RedeSocial)
class RedeSocialAdmin(admin.ModelAdmin):
    list_display = ('link', 'tipo_rede_social')

@admin.register(Contato)
class ContatoAdmin(admin.ModelAdmin):
    list_display = ('contato', 'tipo_contato')